package ke.co.webtribe.liquorrx.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import javax.inject.Provider
import javax.inject.Singleton
import javax.inject.Inject

/**
 * Created by ken on 3/16/18.
 */

@Singleton
class ViewModelProviderFactory @Inject constructor(private val creators: Map<Class<out ViewModel>, @JvmSuppressWildcards
        Provider<ViewModel>>) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val creator = creators[modelClass] ?: creators.asIterable().firstOrNull {
            modelClass.isAssignableFrom(it.key)
        } ?.value ?: throw IllegalArgumentException("unknown model class $modelClass")

        @Suppress("UNCHECKED_CAST")
        return try {
            creator.get() as T
        }catch (e: Exception) {
            throw RuntimeException(e)
        }
    }
}