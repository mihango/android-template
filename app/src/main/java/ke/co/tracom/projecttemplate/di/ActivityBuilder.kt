package com.jambopay.nhif.di


import dagger.Module
import dagger.android.ContributesAndroidInjector
import ke.co.tracom.projecttemplate.ui.activities.MainActivity

/**
 * Created by ken on 3/27/18.
 */
@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(FragmentBuilderModule::class)])
    abstract fun contributesMainActivity(): MainActivity
}