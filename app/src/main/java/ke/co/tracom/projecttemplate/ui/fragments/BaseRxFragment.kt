package ke.co.tracom.projecttemplate.ui.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import com.jambopay.nhif.util.toast
import io.reactivex.disposables.CompositeDisposable
import ke.co.tracom.projecttemplate.R

/**
 * Created by ken on 3/27/18.
 */
open class BaseRxFragment : Fragment() {

    val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onDestroy() {
        super.onDestroy()
//        if(!compositeDisposable.isDisposed) {
//            compositeDisposable.dispose()
//        }
        compositeDisposable.clear()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.window?.setBackgroundDrawableResource(R.drawable.ic_bg)
    }

    fun showError(error: String) {
        if(error.isEmpty()) {
            "An error occurred".toast(context as Context)
        } else {
            error.toast(context as Context)
        }
    }
}