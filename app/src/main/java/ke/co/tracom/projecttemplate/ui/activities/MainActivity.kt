package ke.co.tracom.projecttemplate.ui.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import ke.co.tracom.projecttemplate.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
