package ke.co.webtribe.liquorrx.di

import android.arch.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.multibindings.IntoMap
import android.arch.lifecycle.ViewModel
import dagger.Module
import ke.co.tracom.projecttemplate.viewmodel.DashViewModel


/**
 * Created by ken on 3/16/18.
 */
@Module
abstract class ViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(DashViewModel::class)
    internal abstract fun bindDashViewModel(userViewModel: DashViewModel): ViewModel


    @Binds internal abstract fun bindViewModelFactory(factory: ViewModelProviderFactory): ViewModelProvider.Factory
}