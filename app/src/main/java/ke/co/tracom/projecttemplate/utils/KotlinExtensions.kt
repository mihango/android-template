package com.jambopay.nhif.util

import android.content.Context
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

/**
 * Created by ken on 3/27/18.
 */
fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)
}

fun String.toast(context: Context) {
    Toast.makeText(context, this, Toast.LENGTH_LONG).show()
}

fun String.showFinish(context: Context, handler: () -> Unit, title: String = "Message", cancellable: Boolean = false) {
    val dialog = AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(this)
            .setPositiveButton("OK", { dialog, _ ->
                handler()
                dialog.dismiss()
            })
            .create()
    if(cancellable) dialog.setCancelable(cancellable) else dialog.setCancelable(cancellable)
    dialog.show()
}