package com.jambopay.nhif.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import ke.co.tracom.projecttemplate.utils.USER_PREF
import ke.co.webtribe.liquorrx.di.ViewModelsModule
import javax.inject.Singleton

/**
 * Created by ken on 3/27/18.
 */

@Module(includes = [(NetworkModule::class), (ViewModelsModule::class), (PersistenceModule::class)])
class AppModule {

    @Singleton
    @Provides
    fun provideSharedPreference(app: Application): SharedPreferences =
        app.getSharedPreferences(USER_PREF, Context.MODE_PRIVATE)
}