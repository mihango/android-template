package ke.co.tracom.projecttemplate.ui.fragments

import android.support.v4.app.Fragment
import com.jambopay.nhif.di.Injectable

class DashFragment : Fragment(), Injectable {

    companion object {
        fun newInstance() = DashFragment()
    }
}