package com.jambopay.nhif.persistence

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

/**
 * Created by ken on 3/29/18.
 */
@Database(entities = [], version = 1)
abstract class NhifDatabase : RoomDatabase() {

    abstract fun getAccountDao(): AccountsDAO
}