package ke.co.tracom.projecttemplate.api

import io.reactivex.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST

interface ApiService {

    @FormUrlEncoded
    @POST("token")
    @Headers("no-auth: true")
    fun login(
            @Field("grant_type") grantType: String,
            @Field("username") userName: String,
            @Field("password") password: String
    ): Single<Any>

    @FormUrlEncoded
    @POST("api/payments/post")
    fun prepareEslip(
            @Field("AccountNumber") accountNo: String,
            @Field("PhoneNumber") phone: String
    ): Single<Any>
}