package com.jambopay.nhif.util

import ke.co.tracom.projecttemplate.ui.activities.MainActivity
import ke.co.tracom.projecttemplate.R
import ke.co.tracom.projecttemplate.ui.fragments.DashFragment
import ke.co.tracom.projecttemplate.utils.DASH_FRAG_TAG
import javax.inject.Inject

/**
 * Created by ken on 3/27/18.
 */
class NavigationController @Inject constructor(mainActivity: MainActivity) {

    private val resourceId = R.id.holder
    private val fragmentManger = mainActivity.supportFragmentManager


    fun navigateToDash() {
        if(fragmentManger.backStackEntryCount > 0) {
            for (frag in 0 until fragmentManger.backStackEntryCount)
                fragmentManger.popBackStack()
        }

        fragmentManger.beginTransaction().replace(resourceId, DashFragment.newInstance(), DASH_FRAG_TAG)
                .commitAllowingStateLoss()
    }
}