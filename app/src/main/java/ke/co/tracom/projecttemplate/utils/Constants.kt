package ke.co.tracom.projecttemplate.utils

//prefs
const val USER_PREF = "user"
const val ACCESS_TOKEN = "authToken"
const val IS_LOGGED_IN = "isLoggedIn"


//api
const val BASE_URL = ""

//di
const val RETROFIT_NAME = "retrofitNAme"

//navigation TAGS
const val DASH_FRAG_TAG = "dashboard"