package com.jambopay.nhif.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ke.co.tracom.projecttemplate.ui.fragments.DashFragment

/**
 * Created by ken on 3/12/18.
 */
@Module
abstract class FragmentBuilderModule {

    @ContributesAndroidInjector
    abstract fun bindDashFragment(): DashFragment

}