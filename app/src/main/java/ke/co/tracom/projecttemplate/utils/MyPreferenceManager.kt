package ke.co.tracom.projecttemplate.utils

import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by ken on 3/27/18.
 */
@Singleton
class MyPreferenceManager @Inject constructor(private val preferences: SharedPreferences) {

    val isLoggedIn: Boolean?
        get() = preferences.getBoolean(IS_LOGGED_IN, false)

    val authToken: String
        get() {
            val token = preferences.getString(ACCESS_TOKEN, "")
            return if ((token as String).equals("", ignoreCase = true)) {
                ""
            } else {
                "bearer $token"
            }
        }

    fun loginSuccess(data: Object) {
        preferences.edit()
                .putString("", data.toString())
                .apply()
    }

    fun clearPreference() {
        preferences.edit().clear().apply()
    }
}