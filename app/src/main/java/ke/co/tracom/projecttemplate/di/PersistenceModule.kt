package com.jambopay.nhif.di

import android.app.Application
import android.arch.persistence.room.Room
import com.jambopay.nhif.persistence.NhifDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by ken on 3/29/18.
 */
@Module
class PersistenceModule {

    @Singleton
    @Provides
    fun providesRoomDatabase(app: Application) = Room.databaseBuilder(app,
            NhifDatabase::class.java, "nhif-database").build()

    @Singleton
    @Provides
    fun provideAccountsDao(database: NhifDatabase) = database.getAccountDao()
}