package ke.co.tracom.projecttemplate.api

import ke.co.tracom.projecttemplate.BuildConfig
import ke.co.tracom.projecttemplate.utils.MyPreferenceManager
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by ken on 4/19/18.
 */
@Singleton
class AuthInterceptor @Inject constructor(private val myPreferenceManager: MyPreferenceManager) : Interceptor {

    override fun intercept(chain: Interceptor.Chain?): Response {
        val request: Request = chain!!.request()
        return chain.proceed(if(request.header("no-auth") == null) {
            request.newBuilder()
                    .addHeader("app_key", BuildConfig.AppKey)
                    .addHeader("Authorization", myPreferenceManager.authToken)
                    .build()
        }else {
            request.newBuilder()
                    .addHeader("app_key", BuildConfig.AppKey)
                    .build()
        })
    }
}