package ke.co.webtribe.liquorrx.di

import dagger.MapKey
import android.arch.lifecycle.ViewModel
import kotlin.reflect.KClass


/**
 * Created by ken on 3/16/18.
 */

@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@MapKey
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)